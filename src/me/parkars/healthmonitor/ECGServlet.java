package me.parkars.healthmonitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.parkars.healthmonitor.models.ECG;

import com.google.appengine.api.datastore.Text;

@SuppressWarnings("serial")
public class ECGServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.setHeader("Access-Control-Allow-Origin", "*");
		PrintWriter responseWriter = resp.getWriter();

		Text ecg = new Text(req.getParameter("ecg"));

		if (ecg != null) {
			ECG mEcg = new ECG(ecg);
			mEcg.addDataToDS();
		} else {
			responseWriter.write("No ECG parameter");
		}
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// resp.setContentType("text/plain");
		 resp.setHeader("Access-Control-Allow-Origin", "*");
		 resp.setContentType("application/json");
		 BufferedReader mreader = new BufferedReader(new InputStreamReader(req.getInputStream()));
		StringBuilder mBuilder = new StringBuilder();
		String dataLine = mreader.readLine();
//		while ( != null) {
			System.out.println(dataLine);
			mBuilder.append(dataLine);
			System.out.println("M Bilder" +mBuilder.toString());
//		}
		
		PrintWriter responseWriter = resp.getWriter();
		Text ecg = new Text(mBuilder.toString());
		System.out.println("ECG VAL - "+ecg.getValue());

		if (ecg != null) {
			ECG mEcg = new ECG(ecg);
			mEcg.addDataToDS();
		} else {
			responseWriter.write("No ECG parameter");
		}
	}
	
//	@Override
//	public void doPost(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		// resp.setContentType("text/plain");
//		 resp.setHeader("Access-Control-Allow-Origin", "*");
//		 resp.setContentType("application/json");
//		 JSONObject jObj;
//		try {
//			jObj = new JSONObject(req.getParameter("mydata"));
//			System.out.println(jObj.toString());
//			 Iterator it = jObj.keys();
//			 
//				Text ecg = new Text(jObj.getString("ecg"));
//				PrintWriter responseWriter = resp.getWriter();
//				responseWriter.write(ecg.getValue());
//
//				if (ecg != null) {
//					ECG mEcg = new ECG(ecg);
//					mEcg.addDataToDS();
//				} else {
//					responseWriter.write("No ECG parameter");
//				}
//			
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} // this parses the 
//
//	}
}
