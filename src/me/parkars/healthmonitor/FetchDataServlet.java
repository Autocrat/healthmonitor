package me.parkars.healthmonitor;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.parkar.healthmonitor.helper.PMF;
import me.parkars.healthmonitor.models.ECG;
import me.parkars.healthmonitor.models.HeartRate;
import me.parkars.healthmonitor.models.Temperature;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class FetchDataServlet extends HttpServlet {

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(HeartRate.class);
		query.setFilter("deviceKey==devId");
		query.declareParameters("String devId");

		String heartRate = ((List<HeartRate>) query.execute("KEY_HEART_RATE")).get(0)
				.getHeartRate();
		boolean failed = false;

		JSONObject data = new JSONObject();
		if (heartRate == null) {
			failed = true;
			// resp.getWriter().println("No Heart Rate Values");
		} else {
			try {
				data.append("BPM", heartRate);
			} catch (JSONException e) {
				failed = true;
				e.printStackTrace();
			}
			// resp.getWriter().println("Heart Rate : " + heartRate + " BPM");
		}

		query = pm.newQuery(Temperature.class);
		query.setFilter("deviceKey==devId");
		query.declareParameters("String devId");

		String temperature = ((List<Temperature>) query.execute("KEY_TEMP")).get(0)
				.getTemperature();

		if (temperature == null) {
			failed = true;
			// resp.getWriter().println("No temperature Values");
		} else {
			// resp.getWriter().println("Temperature : " + temperature + " ºC");
			try {
				data.append("TEMP", temperature);
			} catch (JSONException e) {
				failed = true;
				e.printStackTrace();
			}
		}

		query = pm.newQuery(ECG.class);
		query.setFilter("deviceKey==devId");
		query.declareParameters("String devId");

		Text ecg = ((List<ECG>) query.execute("KEY_ECG")).get(0).getEcgVal();
		System.out.println("Data - "+ecg.getValue());
		if (ecg == null) {
			failed = true;
			// resp.getWriter().println("No ECG Values");
		} else {
			// resp.getWriter().println("ECG : " + ecg);
			String mEcg = ecg.getValue();
			String[] ecgArray = mEcg.split("\\|");
			JSONArray ecgJsonArray = new JSONArray();
			for (int i = 0; i < ecgArray.length; i++) {
				try {
					ecgJsonArray.put(i, ecgArray[i]);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			try {
				data.append("ECG", ecgJsonArray);
			} catch (JSONException e) {
				failed = true;
				e.printStackTrace();
			}
		}

		if (!failed) {
			resp.getWriter().println(data.toString());
			resp.setStatus(HttpServletResponse.SC_ACCEPTED);
		} else {
			resp.getWriter().println("Failed to retrieve data");
			resp.sendError(HttpServletResponse.SC_EXPECTATION_FAILED);
		}

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		throw new UnsupportedOperationException();
	}

}
