package me.parkars.healthmonitor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.parkars.healthmonitor.models.HeartRate;
import me.parkars.healthmonitor.models.Temperature;

@SuppressWarnings("serial")
public class HeartRateServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.setHeader("Access-Control-Allow-Origin", "*");
		PrintWriter responseWriter = resp.getWriter();
		String hrtRate = req.getParameter("hrt");
		
		if(hrtRate != null && !hrtRate.isEmpty()) {
		HeartRate mHrtRate = new HeartRate(hrtRate);
		mHrtRate.addDataToDS();
		} else {
			responseWriter.write("No Temperature parameter");
		}
	}
}
