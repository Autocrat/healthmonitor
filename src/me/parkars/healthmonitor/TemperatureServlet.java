package me.parkars.healthmonitor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.*;

import me.parkars.healthmonitor.models.Temperature;

@SuppressWarnings("serial")
public class TemperatureServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.setHeader("Access-Control-Allow-Origin", "*");
		PrintWriter responseWriter = resp.getWriter();
		String temperature = req.getParameter("temp");

		if (temperature != null && !temperature.isEmpty()) {
			responseWriter.print("Temperature "+temperature);
			Temperature mTemp = new Temperature(temperature);
			mTemp.addDataToDS();
		} else {
			responseWriter.write("No Temperature parameter");
		}
	}
}
