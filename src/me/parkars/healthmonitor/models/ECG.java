package me.parkars.healthmonitor.models;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import me.parkar.healthmonitor.helper.PMF;

import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class ECG {
	@PrimaryKey
	private String deviceKey;
 
	@Persistent
	private Text ecgVal;

	public Text getEcgVal() {
		return ecgVal;
	}

	public void setEcgVal(Text ecgVal) {
		this.ecgVal = ecgVal;
	}

	public ECG(Text ecgVal) {
		this.deviceKey = "KEY_ECG";
		this.ecgVal = ecgVal;
	}

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}

	public void addDataToDS() {

		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(this);
		} finally {
			pm.close();
		}
	}

}
