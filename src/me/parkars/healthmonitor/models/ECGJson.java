package me.parkars.healthmonitor.models;

public class ECGJson {
	private String ecg;

	public ECGJson(String ecg) {
		super();
		this.ecg = ecg;
	}

	public String getEcg() {
		return ecg;
	}

	public void setEcg(String ecg) {
		this.ecg = ecg;
	}
	
}
