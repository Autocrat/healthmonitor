package me.parkars.healthmonitor.models;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import me.parkar.healthmonitor.helper.PMF;

@PersistenceCapable
public class HeartRate {
	@PrimaryKey
	private String deviceKey;

	@Persistent
	private String heartRate;

	public String getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(String heartRate) {
		this.heartRate = heartRate;
	}

	public HeartRate(String heartRate) {
		this.deviceKey = "KEY_HEART_RATE";
		this.heartRate = heartRate;
	}

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}

	public void addDataToDS() {

		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(this);
		} finally {
			pm.close();
		}
	}

}
