package me.parkars.healthmonitor.models;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import me.parkar.healthmonitor.helper.PMF;

@PersistenceCapable
public class Temperature {
	@PrimaryKey
	private String deviceKey;

	@Persistent
	private String temperature;

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public Temperature(String temperature) {
		this.deviceKey = "KEY_TEMP";
		this.temperature = temperature;
	}

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}

	public void addDataToDS() {

		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(this);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
	}

}
