<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>

<title>Health Monitor</title>
<script type="text/javascript">
	$(function() {
		$("#header").load("header.html");
	});
	


</script>

</head>
<body>
	<div id="header"></div>
	<form style='padding: 10px'>
		<div class='row-md-12'>
			<h4>
				<label class='col-md-1' style='padding: 5px'>Temperature</label>
			</h4>
			<div class='col-md-6'>
				<div class='col-md-3'>
					<div
						style='background-color: white; width: 150px; height: 30px; padding: 5px; border: 2px solid black;'>
						<label id='tempValue'></label>
					</div>

				</div>
				<div class='col-md-3'>
					<input type="button" class="btn-primary" id="download" onclick= "window.location.href ='/history.html'" value="Download data"></button>
				</div>
			</div>
		</div>


		<h4>
			<label class='col-md-1' style='padding: 5px; visibility: hidden'>Heart
				Rate</label>
		</h4>
		<div class='col-md-3' style='visibility: hidden'>
			<div
				style='background-color: white; width: 150px; height: 30px; padding: 5px; border: 2px solid black;'>
				<label id='bpmValue'></label>
			</div>
		</div>
	</form>

	<script type="text/javascript"
		src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>

	<script>
		google.load('visualization', '1', {
			packages : [ 'corechart' ]
		});
		google.setOnLoadCallback(fetchData);

		function fetchData() {
			$.ajax({
				type : "POST",
				url : "/healthmonitor",
				success : function(result) {
					loadChart(result)
				}
			});
		}

		function loadChart(jsonData) {
			var parsedJson = $.parseJSON(jsonData);
			console.log(parsedJson);
			var ecgData = parsedJson.ECG;
			var bpm = parsedJson.BPM;
			var tempData = parsedJson.TEMP;
			var ecgSplit = String(ecgData).split(",");
			$('#tempValue').text(tempData + " \xB0C");
			$('#bpmValue').text(bpm + " BPM");
			$('#val').text(ecgSplit.length);

			var dataG = new google.visualization.DataTable();
			dataG.addColumn('number');
			dataG.addColumn('number', "HeartRate");

			for (var i = 0; i < ecgSplit.length; i++) {
				dataG.addRow([ parseInt(i), parseInt(ecgSplit[i]) ]);
				//console.log(i + " : " + ecgSplit[i]);
			}

			var options = {
				width : 2000,
				height : 563,
				hAxis : {
					title : 'ECG Time'
				},
				vAxis : {
					title : 'Amplitude'
				}
			};

			var chart = new google.visualization.LineChart(document
					.getElementById('ecgChart'));

			chart.draw(dataG, options);

		}
	</script>

	<div class='col-sm-11'>
		<div id='ecgChart'></div>
	</div>
</body>
</html>